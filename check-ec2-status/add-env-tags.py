import boto3

ec2_client_sydney = boto3.client('ec2', region_name="ap-southeast-1")
ec2_resource_sydney = boto3.resource('ec2', region_name="ap-southeast-1")
instance_ids_sydney = []

reservations_sydney = ec2_client_sydney.describe_instances()['Reservations']

for reservation in reservations_sydney:
  for instance in reservation['Instances']:
    if instance['State']['Name'] == 'running':
      instance_ids_sydney.append(instance['InstanceId'])  

response = ec2_resource_sydney.create_tags(
  Resources=instance_ids_sydney,
  Tags=[
    {
      'Key': 'environment',
      'Value': 'TWwN-Python-dev'
    }
  ]
)