#requires boto3 and schedule package from pip
import schedule
import boto3

ec2_client = boto3.client('ec2', region_name="ap-southeast-1")
ec2_resource = boto3.resource('ec2', region_name="ap-southeast-1")

def check_instance_status():
  statuses = ec2_client.describe_instance_status()
  for status in statuses['InstanceStatuses']:
    instance_status = status['InstanceStatus']['Status']
    instance_state = status['InstanceState']['Name']
    system_status = status['SystemStatus']['Status']
  
    print(f"Instance: {status['InstanceId']} is {instance_state}, Status: {instance_status}, System: {system_status}")
    
schedule.every(20).seconds.do(check_instance_status)

while True:
  schedule.run_pending()