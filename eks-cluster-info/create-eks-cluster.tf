data "aws_ami" "latest_amzn_linux" {
  most_recent = true
  owners      = ["amazon"]

  # further filter the qualified images.
  filter {
    name   = "name" # the property to use to filter, check "aws cli" to check what propety you can use to refer by querying the ami objects (aws ec2 describe-images).
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  # filter can be speficied multiple times for a different criteria
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.17.2"

  cluster_name                   = var.cluster_name
  cluster_version                = "1.28"
  cluster_endpoint_public_access = true

  subnet_ids = module.vpc.private_subnets
  vpc_id     = module.vpc.vpc_id
  tags = {
    environment = "development"
    application = "myapp"
  }

  # worker nodes
  eks_managed_node_groups = {
    dev = {
      use_custom_templates = false
      instance_types       = ["t2.medium"]

      min_size     = 1
      max_size     = 3
      desired_size = 1

      #ami_id = data.aws_ami.latest_amzn_linux.id

      tags = {
        node = "eks-ng"
      }
    }
  }
}
