import boto3

client = boto3.client('eks', region_name="ap-southeast-1")
clusters = client.list_clusters()['clusters']

for cluster in clusters:
  response = client.describe_cluster(name=cluster)
  
  print(f"Cluster {cluster} status is {response['cluster']['status']}")
  print(f"Cluster endpoint: {response['cluster']['endpoint']}")
  print(f"Cluster version: {response['cluster']['version']}")