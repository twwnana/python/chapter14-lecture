import requests
import smtplib
import os
import paramiko

def sendNotification(msg):
  with smtplib.SMTP('smtp-mail.outlook.com', 587) as smtp:
    smtp.starttls()
    smtp.ehlo()
    smtp.login(os.environ.get("EMAIL_ADDRESS"), os.environ.get("EMAIL_PASSWORD"))
    
    smtp.sendmail(
      os.environ.get("EMAIL_ADDRESS"),
      "sheenismhael.lim@gmail.com",
      msg
    )
    smtp.close()
    
def restartAppContainer():
  ssh_client = paramiko.SSHClient()
  ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  ssh_client.connect(hostname='devops.itfellasgroup.com', port=22, username="root", key_filename="/home/sheenlim08/.ssh/id_rsa")
  stdin, stdout, stderr = ssh_client.exec_command('docker restart ca420aeb7811')
  
  ssh_client.close()
    
endpoint = "http://devops.itfellasgroup.com:8081"
try:
  response = requests.get(endpoint)

  if response.status_code == 200:
    print("Application is accessible at port 8079 is up.")
  else:
    print("Application is Down.")
    msg = f"Subject: SITE DOWN\nPlease check ASAP.\n\nApplication Returned {response.status_code}. Attempting to restart container."
    sendNotification(msg)
    restartAppContainer()
    
except:
  msg = f"Subject: SITE DOWN\n\n"+"Looks like Server is not returning anything. Attempting to restart container."
  sendNotification(msg)
  restartAppContainer()