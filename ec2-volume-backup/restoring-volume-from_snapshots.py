import boto3
from operator import itemgetter

ec2_client = boto3.client('ec2', region_name="ap-southeast-1")
ec2_resource = boto3.resource('ec2', region_name="ap-southeast-1")

instance_id = "i-0d9d9b2d0714d1e72"

volumes = ec2_client.describe_volumes(
  Filters=[{
    'Name': 'attachment.instance-id',
    'Values': [instance_id]
  }]
)

instance_volume = volumes['Volumes'][0]

snapshots = ec2_client.describe_snapshots(
  OwnerIds=["self"],
  Filters=[{
    'Name': 'volume-id',
    'Values': [instance_volume['VolumeId']]
  }]
)

# sort the snapshots collection using the key name 'StartTime'
latest_snapshot = sorted_by_date = sorted(snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)[0]

# create a new volume based on the latest snapshot
restored_volume = ec2_client.create_volume(
  SnapshotId=latest_snapshot['SnapshotId'],
  AvailabilityZone="ap-southeast-1b",
  TagSpecifications=[{
    'ResourceType': 'volume',
    'Tags': [{
      'Key': 'Name',
      'Value': 'prod'
    }]
  }]
)

while True:
  volState = ec2_resource.Volume(restored_volume['VolumeId'])
  if volState.state == 'available':
    ec2_resource.Instance(instance_id).attach_volume(
      VolumeId=restored_volume['VolumeId'],
      Device="/dev/xvdb" # must be a different device from the existing instance
    )
    
    break