import boto3
import schedule

ec2_client = boto3.client('ec2', region_name="ap-southeast-1")

volumes = ec2_client.describe_volumes(
  Filters=[
    {
      'Name': 'tag:Name',
      'Values': ['prod']
    }
  ]
)

def takeSnapshots():
  for volume in volumes['Volumes']:
    ec2_client.create_snapshot(VolumeId=volume['VolumeId'])

takeSnapshots() 

# schedule.every(30).days.do(takeSnapshots)

# while True:
#   schedule.run_pending()