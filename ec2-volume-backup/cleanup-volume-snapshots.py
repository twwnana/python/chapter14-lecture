import boto3
from operator import itemgetter

ec2_client = boto3.client("ec2", region_name="ap-southeast-1")

volumes = ec2_client.describe_volumes(
  Filters=[
    {
      'Name': 'tag:Name',
      'Values': ['prod']
    }
  ]
)
for volume in volumes['Volumes']:
  my_snapshots = ec2_client.describe_snapshots(
    OwnerIds=["self"],
    Filters=[
      {
        'Name': 'volume-id',
        'Values': [volume['VolumeId']]
      }
    ]
  )

  # sort the my_snapshots collection using the key name 'StartTime'
  sorted_by_date = sorted(my_snapshots['Snapshots'], key=itemgetter('StartTime'), reverse=True)

  # skip the first 2 elements in the list
  for snapshot in sorted_by_date[2:]:
    ec2_client.delete_snapshot(SnapshotId=snapshot['SnapshotId'])